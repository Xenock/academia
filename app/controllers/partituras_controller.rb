class PartiturasController < ApplicationController
  before_action :set_partitura, only: [:show, :edit, :update, :destroy]

  # GET /partituras
  # GET /partituras.json
  def index
    @partituras = Partitura.all
  end

  # GET /partituras/1
  # GET /partituras/1.json
  def show
  end

  # GET /partituras/new
  def new
    @partitura = Partitura.new
  end

  # GET /partituras/1/edit
  def edit
  end

  # POST /partituras
  # POST /partituras.json
  def create
    @partitura = Partitura.new(partitura_params)

    respond_to do |format|
      if @partitura.save
        format.html { redirect_to @partitura, notice: 'Partitura was successfully created.' }
        format.json { render :show, status: :created, location: @partitura }
      else
        format.html { render :new }
        format.json { render json: @partitura.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /partituras/1
  # PATCH/PUT /partituras/1.json
  def update
    respond_to do |format|
      if @partitura.update(partitura_params)
        format.html { redirect_to @partitura, notice: 'Partitura was successfully updated.' }
        format.json { render :show, status: :ok, location: @partitura }
      else
        format.html { render :edit }
        format.json { render json: @partitura.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /partituras/1
  # DELETE /partituras/1.json
  def destroy
    @partitura.destroy
    respond_to do |format|
      format.html { redirect_to partituras_url, notice: 'Partitura was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_partitura
      @partitura = Partitura.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def partitura_params
      params.require(:partitura).permit(:titulo, :autor, :estilo)
    end
end
