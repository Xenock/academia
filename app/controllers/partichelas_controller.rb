class PartichelasController < ApplicationController
  before_action :set_partichela, only: [:show, :edit, :update, :destroy]

  # GET /partichelas
  # GET /partichelas.json
  def index
    @partichelas = Partichela.all
  end

  # GET /partichelas/1
  # GET /partichelas/1.json
  def show
  end

  # GET /partichelas/new
  def new
    @partichela = Partichela.new
  end

  # GET /partichelas/1/edit
  def edit
  end

  # POST /partichelas
  # POST /partichelas.json
  def create
    @partichela = Partichela.new(partichela_params)

    respond_to do |format|
      if @partichela.save
        format.html { redirect_to @partichela, notice: 'Partichela was successfully created.' }
        format.json { render :show, status: :created, location: @partichela }
      else
        format.html { render :new }
        format.json { render json: @partichela.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /partichelas/1
  # PATCH/PUT /partichelas/1.json
  def update
    respond_to do |format|
      if @partichela.update(partichela_params)
        format.html { redirect_to @partichela, notice: 'Partichela was successfully updated.' }
        format.json { render :show, status: :ok, location: @partichela }
      else
        format.html { render :edit }
        format.json { render json: @partichela.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /partichelas/1
  # DELETE /partichelas/1.json
  def destroy
    @partichela.destroy
    respond_to do |format|
      format.html { redirect_to partichelas_url, notice: 'Partichela was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_partichela
      @partichela = Partichela.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def partichela_params
      params.require(:partichela).permit(:voz)
    end
end
