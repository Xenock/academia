json.array!(@partichelas) do |partichela|
  json.extract! partichela, :id, :voz
  json.url partichela_url(partichela, format: :json)
end
