json.array!(@partituras) do |partitura|
  json.extract! partitura, :id, :titulo, :autor, :estilo
  json.url partitura_url(partitura, format: :json)
end
