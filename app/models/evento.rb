class Evento < ActiveRecord::Base
  has_many :eventos_partituras
  has_many :partituras, through: :eventos_partituras
end
