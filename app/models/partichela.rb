class Partichela < ActiveRecord::Base
  belongs_to :partitura
  belongs_to :instrumento
  mount_uploader :archivo, ArchivoUploader
end
