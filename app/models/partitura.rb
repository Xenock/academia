class Partitura < ActiveRecord::Base
  has_many :eventos_partituras
  has_many :eventos, through: :eventos_partituras
  has_many :partichelas
end
