Rails.application.routes.draw do
  devise_for :users
  root 'admin#index'

  get 'admin', to: 'admin#index'
  scope 'admin' do
    resources :instrumentos, :partichelas, :partituras, :eventos
  end
end
