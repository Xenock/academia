require 'test_helper'

class PartichelasControllerTest < ActionController::TestCase
  setup do
    @partichela = partichelas(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:partichelas)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create partichela" do
    assert_difference('Partichela.count') do
      post :create, partichela: { voz: @partichela.voz }
    end

    assert_redirected_to partichela_path(assigns(:partichela))
  end

  test "should show partichela" do
    get :show, id: @partichela
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @partichela
    assert_response :success
  end

  test "should update partichela" do
    patch :update, id: @partichela, partichela: { voz: @partichela.voz }
    assert_redirected_to partichela_path(assigns(:partichela))
  end

  test "should destroy partichela" do
    assert_difference('Partichela.count', -1) do
      delete :destroy, id: @partichela
    end

    assert_redirected_to partichelas_path
  end
end
