require 'test_helper'

class PartiturasControllerTest < ActionController::TestCase
  setup do
    @partitura = partituras(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:partituras)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create partitura" do
    assert_difference('Partitura.count') do
      post :create, partitura: { autor: @partitura.autor, estilo: @partitura.estilo, titulo: @partitura.titulo }
    end

    assert_redirected_to partitura_path(assigns(:partitura))
  end

  test "should show partitura" do
    get :show, id: @partitura
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @partitura
    assert_response :success
  end

  test "should update partitura" do
    patch :update, id: @partitura, partitura: { autor: @partitura.autor, estilo: @partitura.estilo, titulo: @partitura.titulo }
    assert_redirected_to partitura_path(assigns(:partitura))
  end

  test "should destroy partitura" do
    assert_difference('Partitura.count', -1) do
      delete :destroy, id: @partitura
    end

    assert_redirected_to partituras_path
  end
end
