class CreatePartituras < ActiveRecord::Migration
  def change
    create_table :partituras do |t|
      t.string :titulo
      t.string :autor
      t.string :estilo

      t.timestamps null: false
    end
  end
end
