class CreateEventosPartituras < ActiveRecord::Migration
  def change
    create_table :eventos_partituras do |t|
      t.references :evento, index: true, foreign_key: true
      t.references :partitura, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
