class CreatePartichelas < ActiveRecord::Migration
  def change
    create_table :partichelas do |t|
      t.string :voz

      t.timestamps null: false
    end
  end
end
