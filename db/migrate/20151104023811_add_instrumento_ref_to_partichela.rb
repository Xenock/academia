class AddInstrumentoRefToPartichela < ActiveRecord::Migration
  def change
    add_reference :partichelas, :instrumento, index: true, foreign_key: true
  end
end
