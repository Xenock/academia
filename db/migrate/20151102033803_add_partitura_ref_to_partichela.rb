class AddPartituraRefToPartichela < ActiveRecord::Migration
  def change
    add_reference :partichelas, :partitura, index: true, foreign_key: true
  end
end
